1. Создание контейнера с базой данных PostgreSQL

docker run --name docker_bd -e POSTGRES_USER=u_test -e POSTGRES_PASSWORD=p_test -e POSTGRES_DB=test_db  -d postgres

2. Создание контейнера с приложением

(docker build -t friendlyhello .)

(docker run -p 4000:80 friendlyhello)

mvn clean package docker:build

3. Посмотреть запущенные контейнеры

docker container ls

4. Посмотреть image

docker image ls

5. Запостить image в репозиторий

docker login

(docker tag app gordon/get-started:part2)

docker push vnvyunnikov/web-jpa:latest

6. Соединить контейнеры и запустить

docker run -it --name final_app --link docker_bd:postgres -p 8080:8080 vnvyunnikov/web-jpa:latest

(docker start final_app3 -a) # включить (-a вывод в консоль)

(docker stop final_app3) # выключить

(docker inspect final_app3) # посмотреть IP

7. Запуск через compose

docker-compose up -d

docker-compose stop

docker ps

docker inspect <id>

docker inspect webjpa_final_application_1
