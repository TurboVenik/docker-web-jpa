package com.example.docker.webjpa.web.rest;

import com.example.docker.webjpa.database.model.HelloEntity;
import com.example.docker.webjpa.database.repository.HelloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HelloController {

    @Autowired
    private HelloRepository helloRepository;

    @RequestMapping("/")
    public List<HelloEntity> hello() {
        return helloRepository.findAll();
    }
}
