package com.example.docker.webjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class WebJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebJpaApplication.class, args);
	}
}
