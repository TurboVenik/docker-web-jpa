package com.example.docker.webjpa.database.service;

import com.example.docker.webjpa.database.model.HelloEntity;
import com.example.docker.webjpa.database.repository.HelloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class HelloService {

    @Autowired
    private HelloRepository helloRepository;

    @PostConstruct
    public void init() {
        HelloEntity helloEntity = new HelloEntity();
        helloEntity.setMessage("Hello_Docker");
        helloRepository.save(helloEntity);
    }
}
