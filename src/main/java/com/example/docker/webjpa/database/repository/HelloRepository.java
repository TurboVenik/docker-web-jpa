package com.example.docker.webjpa.database.repository;

import com.example.docker.webjpa.database.model.HelloEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HelloRepository  extends JpaRepository<HelloEntity, Long> {
}
